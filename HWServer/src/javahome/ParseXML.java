package javahome;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ParseXML {
	static File xml = new File("D:/2.xml");

	Document doc;

	ParseXML() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
		doc = docBuilder.parse(xml);
	}

	public String getPassword(String str) {
		String value;

		for (int i = 0; i < doc.getElementsByTagName("login").getLength(); i++) {
			value = doc.getElementsByTagName("login").item(i).getTextContent();
			if (value.equals(str))
				return getLogin(i);
		}
		return null;

	}

	private String getLogin(int i) {

		return doc.getElementsByTagName("password").item(i).getTextContent();

	}

}
